<?php
/**
 * Created by PhpStorm.
 * User: yancy
 * Date: 16/06/14
 * Time: 18:46
 */

function get_post_string($params, $mask=''){
    foreach ($params as $key => &$val) {
        if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    if (!empty($mask))
        $post_params[] = 'mask=' . urlencode($mask);
    return $post_string = implode('&', $post_params);
}

function curl_post_async($processor, $params, $mask='')
{

    $post_string = get_post_string($params, $mask);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $processor);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, 'curl');
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    $result = curl_exec($ch);
    curl_close($ch);
}

function curl_socket_post_async($url, $params)
{
    foreach ($params as $key => &$val) {
        if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);

    $parts=parse_url($url);

    $fp = fsockopen($parts['host'],
        isset($parts['port'])?$parts['port']:80,
        $errno, $errstr, 30);

    pete_assert(($fp!=0), "Couldn't open a socket to ".$url." (".$errstr.")");

    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out.= "Content-Length: ".strlen($post_string)."\r\n";
    $out.= "Connection: Close\r\n\r\n";
    if (isset($post_string)) $out.= $post_string;

    fwrite($fp, $out);
    fclose($fp);
}


function getURL_old_to_delete($requestData, $client){
    $mask = $requestData['mask'];
    if ($db = new SQLite3('db/cached.db')) {
        $db->exec('CREATE TABLE IF NOT EXISTS email_content_url (ID int, Url varchar(500), PRIMARY KEY (ID))');
        $queryFormat = "SELECT url FROM email_content_url WHERE ID='%s'";
        $insertFormat = "INSERT INTO email_content_url(ID, Url) VALUES (%s, '%s')";

        preg_match_all('/[\\da-f]/', $mask, $matches);

        $hex_string = implode('', $matches[0]);
        $number = hexdec($hex_string);
        $urlID = $number - 17;

        $query = sprintf($queryFormat, $urlID);

        $q = @$db->querySingle($query);

        if ($q === false || $q == null){
            //GET FROM MASTER SERVER
            try{
                $request = $client->post('', null, $requestData, array('timeout' => 59, 'connect_timeout' => 59));
                $url = $request->send()->json();
                $insertQuery = sprintf($insertFormat, $urlID, $url);
                $db->exec($insertQuery);

            } catch (Exception $e) {
                //$logger->error($e->getMessage());
            }
        }else{
            $url = $q;
        }
    }
    return $url;

}

function getURL($requestData, $client, $img = false, $params=NULL, $email){
    global $masterName;
    $mask = $requestData['mask'];
    if ($db = new SQLite3('db/cached.db')) {
        $databaseName = "email_content_url" . ($masterName? "_".$masterName : '');
        $db->exec('CREATE TABLE IF NOT EXISTS '. $databaseName .' (ID int, Url varchar(500), localURL varchar(500), PRIMARY KEY (ID))');
        $queryFormat = (!$img) ? "SELECT url FROM $databaseName WHERE ID='%s'" : "SELECT ID, Url, localURL FROM $databaseName WHERE ID='%s'";
        $insertFormat = (!$img) ? "INSERT INTO $databaseName(ID, Url) VALUES (%s, '%s')" : "INSERT INTO $databaseName(ID, Url, localURL) VALUES (%s, '%s', '%s')";

        preg_match_all('/[\\da-f]+/', $mask, $matches);

        $jobid = hexdec($matches[0][count($matches[0])-1]);

        $requestData['jobID'] = $jobid;

        unset($matches[0][count($matches[0])-1]);

        $hex_string = implode('', $matches[0]);
        $number = hexdec($hex_string);
        $urlID = $number - 17;

        //return array("urlID" => $urlID, "jobID" => $jobid, "mask" => $mask);

        $query = sprintf($queryFormat, $urlID);

        if ($img){
            $q = @$db->querySingle($query, true);
        }else{
            $q = @$db->querySingle($query);
        }

        if ($q === false || $q == null){
            //GET FROM MASTER SERVER
            try{
                $request = $client->post('', null, $requestData, array('timeout' => 59, 'connect_timeout' => 59));
                $url = $request->send()->json();

                if ($img){
                    $randomName = MASTER_SERVER_NAME . "img" . $urlID;
                    $count = 0;
                    do{
                        if ($count++ == 2){
                            $localURL = 'img/eh.gif';
                            break;
                        }
                    }while (!($localURL = saveImage($url, $randomName)));
                    $insertQuery = sprintf($insertFormat, $urlID, $url, $localURL);
                    $db->exec($insertQuery);
                    $url = array("Url" => $url, "localURL" => $localURL);
                    return $url;
                }else{
                    $insertQuery = sprintf($insertFormat, $urlID, $url);
                }
                $db->exec($insertQuery);

            } catch (Exception $e) {
                //$logger->error($e->getMessage());
            }
        }else{
            if ($img){
                $url = $q; //Return only $q['localURL']
                return $url;
            }else{
                $url = $q;
            }
        }
    }

    //Replace the params
    //params
    $regex = "/\\[([^\\[]*)\\]/";
    preg_match_all($regex, $url, $matches);
    $index = 0;
    foreach ($matches[0] as $match){
        //echo "<br>" . $match . "<br>";
        if ($match == "[Subscriber_EmailAddress]"){
            $replace = $email;
        }else{
            $replace = $params[$index++];
        }

        $url = str_replace($match, $replace, $url);
        //var_dump($url);
    }
    return $url;
}

function decrypt_number( $code ){
    preg_match_all('/[\\da-f]/', $code, $matches);

    $hex_string = implode('', $matches[0]);
    $number = hexdec($hex_string);
    $value = $number - 17;
    return $value;
}

function decrypt_script( $code ){
    preg_match_all('/[\\da-f]/', $code, $matches);

    $hex_string = implode('', $matches[0]);
    $number = hexdec($hex_string);
    $value = $number - 17;
    return chr($value);
    //return $value;

    //String integerString = Integer.toHexString((int) ((char) footPrint.charAt(0)) + PAD_NUMBER).toLowerCase();
}

function clearName($code){
    preg_match_all('/[\\da-z]/', $code, $matches);
    return implode('', $matches[0]);
}

function decrypt ( $url ){

    $url_array = explode("/", $url);
   // echo "url: " . json_encode( $url_array ) . "<br>";

    $i = -1;
    foreach ($url_array as $code){
        $i++;
        if (strlen($code) < 7){
            continue;
        }

        $codeArray = explode("E", $code);
        $code = $codeArray[0];

        $clue = decrypt_number(substr($code, 0, 3));
        $clue %= 30;
        $cad = substr($code, 0, 3);
        //echo "characters: $cad, clue: $clue i: $i <br>";
        if ($clue == $i){
            //echo "pre fijo: " . substr($code, 2, 4) . "<br>";
            $pref = decrypt_script(substr($code, 3, 4));
            //echo "suf fijo: " . substr($code, count($code) - 3) . "<br>";
            $suf = decrypt_number(substr($code, count($code) - 4));
            $suf %= 30;
            $mask = substr($code, 7, ($suf-7));
            $hashID =  $suf;
            $cs = substr($code, $suf, count($code) - 4);
            $urlID = decrypt_number($mask);

            //Decode the rest of parameters
            $params = array();
            if (count($codeArray) > 1){
                $count = 0;
                foreach($codeArray as $segment){
                    if ($count++ == 0) continue;
                    $decode = decodeEmail($segment, true);
                    array_push($params, ($decode=='88' ? '': $decode));
                }
            }

            //echo "clue: $clue - pref: $pref - suf: $suf - mask: $mask - cs: $cs - urlID: $urlID";
            return array("clue" => $clue, "pref" => $pref, "urlID" => $urlID, "mask" => $mask, "hashID" => $hashID, "cs" => $cs, "params" => $params);
        }
    }
    return false;
}

function decryptChunk($s){
    $cad = '';
    $i = 0;

    $c = 0;
    while ($i < strlen($s) && $c++ <= strlen($s)) {
        $sub = '';
        // Get length
        //echo "character " . $s[$i] . "<br>";
        while ($i < strlen($s) && is_numeric($s[$i])) {
            //echo "here";
            $sub .= $s[$i];
            $i++;
        }

        $len = intval($sub);

        //Forget first character
        $str = substr($s, $i + 1, $len-1);

        $cad .= $str . '**';
        $i = $i + $len;
    }

    $returnArray = explode("**", rtrim($cad, "**"));
    $version = decrypt_number($returnArray[0]);

    if ($version != 551){
        return null;
    }else{
        //unset($returnArray[0]);
        return $returnArray;
    }
}

function decryptv2 ( $url ){
    global $TRACK_KEYS;

    $url_array = explode("/", $url);
    // echo "url: " . json_encode( $url_array ) . "<br>";

    $i = -1;
    foreach ($url_array as $code){
        $i++;
        if (strlen($code) < 7){
            continue;
        }

        $clue = decrypt_number(substr($code, 0, 3));
        $clue %= 30;
        if ($clue == $i){
            $decodeArray = decryptChunk(substr($code, 3));

            if ($decodeArray == null) return null;

            $pref = decrypt_script($decodeArray[1]);
            $mask = $decodeArray[2];
            //$hashID =  decrypt_number($decodeArray[2]);
            $cs = $decodeArray[3];
            $urlID = decrypt_number($mask);


            //Decode track params
            $trackParams = array();
            if (count($decodeArray) > 4){
                $codeArray = array_slice($decodeArray, 4);

                foreach($TRACK_KEYS as $key => $value){
                    $decode = $codeArray[$key];
                    //var_dump($decode);

                    preg_match_all('/[\\da-f]+/', $decode, $matches);

                    $hex_string = implode('', $matches[0]);

                    //var_dump($hex_string);

                    $number = hexdec($hex_string);
                    $decode = $number - 17;

                    $trackParams[$value] = ($decode=='88' ? '': $decode);
                }               
            }

            //var_dump($trackParams);

            //Decode the rest of parameters
            $from = 4 + count($TRACK_KEYS);
            $params = array();
            if (count($decodeArray) > $from){
                $count = 0;
                $codeArray = array_slice($decodeArray, $from);

                //var_dump($codeArray);

                foreach($codeArray as $segment){
                    //if ($count++ == 0) continue;
                    //echo "here decoding $segment <br>";
                    $decode = decodeEmail($segment);
                    array_push($params, ($decode=='88' ? '': $decode));
                }

                //var_dump($params);
            }

            //echo "clue: $clue - pref: $pref - suf: $suf - mask: $mask - cs: $cs - urlID: $urlID";
            return array("clue" => $clue, "pref" => $pref, "urlID" => $urlID, "mask" => $mask, "cs" => $cs, "trackParams" => $trackParams, "params" => $params);
        }
    }
    return false;
}

function decodeEmail( $cad, $old=false ){
    $MASK_PADDING_CHARACTERS_OLD = "ABCDeFGH@JKLMNOPQRSTUVWXYZabcd&fghijklmnopqrstuvwxyz01234-56789I_.";
	$CODED_MASK_PADDING_OLD      = "89FGH@IJKLabcdeABCDEtuvwxyzMNOPmnopqrsQRS0fghi-jkl1.23TUVW4_567XYZ";

    $MASK_PADDING_CHARACTERS = "ABC@DEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345678.9";
    $CODED_MASK_PADDING      = "GHYZa.bcdeK9LMNOI567JWXPQfghij48@klmRST012UVnopqrstABCDEFuvwxyz3";


    if ($old){
        $MASK_PADDING_CHARACTERS = $MASK_PADDING_CHARACTERS_OLD;
        $CODED_MASK_PADDING = $CODED_MASK_PADDING_OLD;
    }

    $res = "";
    $cad_array = str_split($cad);

    foreach ($cad_array as $c){
        //echo "Character " . $c . " pos " . strpos($MASK_PADDING_CHARACTERS, $c) . "<br>";
        $pos = strpos($MASK_PADDING_CHARACTERS, $c);
        $res .= ($pos) ? substr($CODED_MASK_PADDING, $pos , 1) : $c;
    }
	return $res;
}

function base64url_encode($data) {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode($data) {
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

function saveImage1($url, $name){
    $ch = curl_init($url);
    $fp = fopen('/usr/local/g250/url-handlers/img'.$name, 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
}

function saveImage($url, $name){
    $url = $url;
    $img = 'img_cache/' . $name;
    $eUrl = getLastEffectiveUrl($url);
    if (exif_imagetype($eUrl)){
        file_put_contents($img, file_get_contents(getLastEffectiveUrl($eUrl)));
    }else{
        return FALSE;
    }
    return $img;
}

function getLastEffectiveUrl($url)
{
    // initialize cURL
    $curl = curl_init($url);
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_FOLLOWLOCATION  => true,
    ));

    // execute the request
    $result = curl_exec($curl);

    // fail if the request was not successful
    if ($result === false) {
        curl_close($curl);
        return null;
    }

    // extract the target url
    $redirectUrl = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
    curl_close($curl);

    return $redirectUrl;
}

function cacheHit($processor, $params_array, $mask){
    global $logger, $masterName;
    if ($db = new SQLite3(dirname(__FILE__). '/db/cached.db')) {
        $databaseName = "email_content_url_hit" . ($masterName? "_".$masterName : '');
        $db->exec('CREATE TABLE IF NOT EXISTS ' . $databaseName.' (id INTEGER PRIMARY KEY AUTOINCREMENT , processor TEXT, params TEXT, mask TEXT)');
        $queryFormat = "SELECT params, mask FROM $databaseName WHERE ID='%s'";
        $insertFormat = "INSERT INTO $databaseName(processor, params, mask) VALUES ('%s', '%s', '%s')";

        try{
            $insertQuery = sprintf($insertFormat, $processor, json_encode($params_array), $mask);
            $db->exec($insertQuery);
            return true;
        }catch(Exception $e){
            //$logger->info($e);
            return false;
        }

    }else{
        return false;
    }
}

function getHits($limit, $master=''){
    //todo
    if ($db = new SQLite3(dirname(__FILE__). '/db/cached.db')) {
        $databaseName = "email_content_url_hit" . ($master ? "_".$master : '');
        $db->exec('CREATE TABLE IF NOT EXISTS ' . $databaseName .' (id INTEGER PRIMARY KEY AUTOINCREMENT , processor TEXT, params TEXT, mask TEXT)');

        $queryFormat = "SELECT id, processor, params, mask FROM $databaseName LIMIT %s";
        try{
            $query = sprintf($queryFormat, $limit);
            $results = $db->query($query);
            $rows = array();
            while ($row = $results->fetchArray(SQLITE3_NUM)) {
                array_push($rows, $row);
            }

            return $rows;
        }catch(Exception $e){
            //$logger->info($e);
            return false;
        }

    }else{
        return false;
    }
}

function getTotal($master=''){
    //todo
    if ($db = new SQLite3(dirname(__FILE__). '/db/cached.db')) {
        $databaseName = "email_content_url_hit" . ($master ? "_".$master : '');
        $db->exec('CREATE TABLE IF NOT EXISTS '.$databaseName.' (id INTEGER PRIMARY KEY AUTOINCREMENT , processor TEXT, params TEXT, mask TEXT)');

        $query = "SELECT count(id) as total FROM $databaseName";
        try{
            $q = @$db->querySingle($query);

            return $q;
        }catch(Exception $e){
            //$logger->info($e);
            return 0;
        }

    }else{
        return false;
    }
}

function dropHit($id, $bulk = false, $master=''){
    if ($db = new SQLite3(dirname(__FILE__). '/db/cached.db')) {
        $databaseName = "email_content_url_hit" . ($master ? "_".$master : '');
        $db->exec('CREATE TABLE IF NOT EXISTS '.$databaseName.' (id INTEGER PRIMARY KEY AUTOINCREMENT , processor TEXT, params TEXT, mask TEXT)');

        if ($bulk){
            $deleteFormat = "DELETE FROM $databaseName WHERE id IN %s";
        }else{
            $deleteFormat = "DELETE FROM $databaseName WHERE id = %s";
        }

        try{
            $query = sprintf($deleteFormat, $id);
            $db->exec($query);
        }catch (Exception $e){
            return false;
        }
    }else{
        return false;
    }
}
