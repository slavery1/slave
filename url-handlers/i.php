<?php

/**
 *
 * Mask Image URL Handler - It handles masked Image URL requests from (Campaign) Email URL Links
 *
 * @author Majid Hameed
 * @since Novemeber 25, 2012
 * @modified Nov 10, 2014 by Guido
 *
 */
require("CachedImage.php");
use Guzzle\Http\Client;

//ini_set('display_errors', 'On');

$scriptName = basename(__FILE__, '.php');


$logger->debug($scriptName . ' - Valid Request - Request Data - ' . json_encode($requestData));

// POST data to be saved in database
//$client = new Client($i_processor);

$client = new Client($g_processor);
try {

    $requestData['requestHeaders']['SCRIPT_NAME']  =  "/".$scriptName . '.php';  //Fix script name

    $response = getURL($requestData, $client, true, $params, $decodeEmail);

    //curl_post_async($i_processor, $_SERVER, $mask);  //To log in the server

    $pixel_img = !isBadImage( $response['Url'], $BAD_IMG_LIST );

    $remoteResponse = $response['Url'];
    $response = $response['localURL'];

    $logger->debug($scriptName . ' - response - ' . $response);

    if ($response=='INVALID-URL') {
        if (LOG_HITS){
            $logger->info($IPAddress . " - GET /" .  $code . ' - ' . 'BOT INVALID LINK - ' . $_SERVER['HTTP_CF_CONNECTING_IP']);
        }

        $response = '/img/eh.gif';
    }

    // FETCH original url
    $originalURL = $response;

    if (LOG_HITS){
        $logger->info($IPAddress . " - GET /" .  $code . ' - ' . 'IMAGE - ' . $originalURL);
    }

    // New part
	
	if ($pixel_img){

		$image = new CachedImage($originalURL);

		$image->alterImage();

		header($image->getHeader());

		$image->getImage();
	}else{
		$path = $originalURL;

		if (is_readable($path)) {
				   $info = getimagesize($path);
				   if ($info !== FALSE) {
					   header("Content-type: {$info['mime']}");
					   readfile($path);
					   exit();
				   }
		}


	}
} catch (Exception $e) {
    //$logger->error($e->getMessage());
}
