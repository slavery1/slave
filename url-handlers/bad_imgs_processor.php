<?php
/**
 * Created by PhpStorm.
 * User: yancy
 * Date: 21/10/15
 * Time: 5:39
 */
ini_set('include_path', dirname(__FILE__));

require_once 'vendor/autoload.php';
require 'constants.php';
require 'processors_constants.php';
require 'common.php';
require 'cron_helper.php';
use Guzzle\Http\Client;

ini_set('display_errors', 'Off');

$scriptName = basename(__FILE__, '.php');
$scriptTitle = "Bad images processor cron - ";

try{
    if(($pid = CronHelper::Lock()) !== FALSE) {
        getFromAllMasters();
        CronHelper::Unlock();
    }
}catch (Exception $e){
    //$logger->info($scriptName . ' - Something wrong when processing - ' . json_encode($e));
}

function getFromAllMasters(){
    global $logger, $MASTER_NAMES, $url_handler_constants, $URL_PROCESSORS, $badImgUrl;
    $fileContent = '';
    if (!empty($MASTER_NAMES)){
        foreach(array_unique($MASTER_NAMES) as $masterName){
            $masterUrlServer = sprintf($url_handler_constants['MASTER_SERVER_URL_POSFIX'], $masterName);
            $badImgUrl = $masterUrlServer . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['BadImgUrl'];
            //sendBulkHits($masterProcessor, $masterName);
            $fileContent .= get_bad_imgs($badImgUrl, $masterName);
        }
    }
    else{
        $fileContent .= get_bad_imgs($badImgUrl);
    }

    writeBadImagesFile($fileContent);

}

function writeBadImagesFile($fileContent){
    if( empty($fileContent) || $fileContent == '' ){
        $fileContent = "BAD_IMGS[]=\"\"";
    }

    if( writeFile(dirname(__FILE__). '/bad_imgs.ini', $fileContent) ){
        return true;
    }
    return false;
}

function get_bad_imgs($badImgUrl, $masterName=''){
    global $logger, $scriptTitle;
    $serverPref = $masterName != '' ? "[$masterName]" : '[OLD_VERSION]';
    $logger->info($scriptTitle .$serverPref . " ". __FUNCTION__. ": Init");
    $fileContent = '';
    try{
        $client = new Client($badImgUrl);
        $request = $client->post('', null, array(), array('timeout' => 59, 'connect_timeout' => 59));
        $data = $request->send()->json();

        $fileContent = '';
        if( !empty($data) ){
            foreach ($data as $url) {
                $fileContent .= "BAD_IMGS[]=\"".$url."\"\n";
            }
            $logger->info($scriptTitle . $serverPref . " Got " . count($data) ." bad image(s)");
        }else{
            $logger->info($scriptTitle . $serverPref . "There are not bad images...");
        }
    }catch (Exception $e){
        $logger->info($scriptTitle .$serverPref . " ". __FUNCTION__. " Couldn't resolve host..." );
    }

    $logger->info($scriptTitle .$serverPref . " ". __FUNCTION__. ": End");
    return $fileContent;
}