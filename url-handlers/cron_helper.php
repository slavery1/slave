<?php
define('LOCK_DIR', '/tmp/');
define('LOCK_SUFFIX', '.lock');

class CronHelper {

    private static $pid;

    function __construct() {}
    function __clone() {}

    private static function isrunning() {
        $pids = explode(PHP_EOL, `ps -e | awk '{print $1}'`);
        if(in_array(self::$pid, $pids))
            return TRUE;
        return FALSE;
    }

    public static function Lock() {
        global $argv, $logger;

        $basename = basename($argv[0]);
        $lock_file = LOCK_DIR.$basename.LOCK_SUFFIX;

        $rmStr = self::Utility($basename);
        $logger->info($rmStr.": Lock File = ".$lock_file);
        if(file_exists($lock_file)) {
            // Is running?
            self::$pid = file_get_contents($lock_file);
            if(self::isrunning()) {
                $logger->info($rmStr.": ProcessID:".self::$pid.", already in progress...");
                return FALSE;
            } else {
                $logger->info($rmStr.": ProcessID:".self::$pid.", previous job died abruptly...");
            }
        }

        self::$pid = getmypid();
        file_put_contents($lock_file, self::$pid);
        $logger->info($rmStr.": ProcessID:".self::$pid.". Lock acquired, processing the job...");
        return self::$pid;
    }

    public static function Unlock() {
        global $argv, $logger;

        $basename = basename($argv[0]);
        $lock_file = LOCK_DIR.$basename.LOCK_SUFFIX;

        $rmStr = self::Utility($basename);
        if(file_exists($lock_file))
            unlink($lock_file);

        $logger->info($rmStr.": ProcessID:".self::$pid.", releasing lock...");
        return TRUE;
    }

    public static function Utility( $basename ){
        if( strstr($basename, 'down_servers') || strstr($basename, 'up_servers') ){
            $rmStr = str_replace('_servers_processor.php',' utility', $basename);
        }else if( strstr($basename, 'nc_autochanger') ){
            $rmStr = str_replace('nc_autochanger_processor.php','Namecheap Autochanger Utility', $basename);
        } else{
            $rmStr = str_replace('_processor.php',' utility', $basename);
        }
        return $rmStr;
    }
}