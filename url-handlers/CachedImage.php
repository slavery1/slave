<?php

class CachedImage {

    public $imgURL = "";
    public $type = FALSE;
    public $img = FALSE;
    public $header = "";
    private $sizex = 0;
    private $sizey = 0;

    /*
    function random_color_part() {
       return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    function random_color() {
       return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }*/

    function random_color(){
       $gd = imagecreatetruecolor($this->sizex, $this->sizey);
       return imagecolorallocate($gd, mt_rand( 0, 255 ), mt_rand( 0, 255 ), mt_rand( 0, 255 ));
    }

    //constructor gets URL, catalogs and gets info
    function CachedImage($url) {
          $this->imageURL = $url;

          if (exif_imagetype($url) == IMAGETYPE_GIF) {
            $this->type = "gif";
            $this->img = imagecreatefromgif($url);
            $this->header = "Content-type: image/gif";
          }
          elseif (exif_imagetype($url) == IMAGETYPE_JPEG) {
            $this->type = "jpeg";
            $this->img = imagecreatefromjpeg($url);
            $this->header = "Content-type: image/jpeg";
          }
          elseif (exif_imagetype($url) == IMAGETYPE_PNG) {
            $this->type = "png";
            $this->img = imagecreatefrompng($url);
            $this->header = "Content-type: image/png";
          }
          else {
            //if it didn't work return false and don't get more info
            return FALSE;
          }
          $this->sizex = imagesx($this->img);
          $this->sizey = imagesy($this->img);
    }

    function getHeader() {
	    return $this->header;
    }

    function alterImage() {
      if ( isset($this->img) ) {
        for ($i=0; $i < mt_rand(2,5); $i++) {
          $color = $this->random_color();
          $x = mt_rand( 0, $this->sizex );
          $y = mt_rand( 0, $this->sizey );
          imagesetpixel ( $this->img, $x , $y , $color );
        }
        return TRUE;
      }
      else {
        return FALSE;
      }
    }

    function getImage() {
        //return imagejpeg($this->img);

        if ( isset($this->type) ) {
                switch ($this->type) {
                        case "jpeg":
                                return imagejpeg($this->img);
                                break;
                        case "png":
                                return imagepng($this->img);
                                break;
                        case "gif":
                                return imagegif($this->img);
                        default:
                                return FALSE;
                }
        }
    }
  }

?>
