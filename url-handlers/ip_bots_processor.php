<?php
/**
 * Created by PhpStorm.
 * User: yancy
 * Date: 21/10/15
 * Time: 5:39
 */

ini_set('include_path', dirname(__FILE__));
require_once 'vendor/autoload.php';
require 'constants.php';
require 'processors_constants.php';
require 'common.php';
require 'cron_helper.php';
use Guzzle\Http\Client;

ini_set('display_errors', 'On');

$scriptName = basename(__FILE__, '.php');
$scriptTitle = "Ip Bots processor cron - ";

try{
    if(($pid = CronHelper::Lock()) !== FALSE) {
        //saveIps($ipBotsUrl);
        getFromAllMasters();
        CronHelper::Unlock();
    }
}catch (Exception $e){
    $logger->info($scriptName . ' - Something wrong when processing - ' . $e->getMessage());
}

function getFromAllMasters(){
    global $logger, $MASTER_NAMES, $url_handler_constants, $URL_PROCESSORS, $ipBotsUrl;
    $fileContent = '';
    if (!empty($MASTER_NAMES)){
        foreach(array_unique($MASTER_NAMES) as $masterName){
            $masterUrlServer = sprintf($url_handler_constants['MASTER_SERVER_URL_POSFIX'], $masterName);
            $ipBotsUrl = $masterUrlServer . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['IPBots'];
            $fileContent .= getBadIps($ipBotsUrl, $masterName);
        }
    }
    else{
        $fileContent .= getBadIps($ipBotsUrl);
    }

    saveIps($fileContent);
}

function getBadIps($ipBotsUrl, $masterName=''){
    global $logger, $scriptTitle;
    $serverPref = $masterName != '' ? "[$masterName]" : '[OLD_VERSION]';
    $logger->info($scriptTitle .$serverPref . " ". __FUNCTION__. ": Init");
    $fileContent = '';
    try{
        $client = new Client($ipBotsUrl);
        $request = $client->post('', null, array(), array('timeout' => 59, 'connect_timeout' => 59));
        $data = $request->send()->json();

        $fileContent = '';
        if( !empty($data) ){
            foreach ($data as $url) {
                $fileContent .= $url."\n";
            }
            $logger->info($scriptTitle . $serverPref . " Got " . count($data) ." bad ips(s)");
        }else{
            $logger->info($scriptTitle . $serverPref . "There are not bad ips...");
        }
    }catch (Exception $e){
        $logger->info($scriptTitle .$serverPref . " ". __FUNCTION__. " Couldn't resolve host..." );
    }

    $logger->info($scriptTitle .$serverPref . " ". __FUNCTION__. ": End");
    return $fileContent;
}

function saveIps($fileContent){
    if( writeFile(dirname(__FILE__). '/bad_ips.txt', $fileContent) ){
        return true;
    }
    return false;
}

function loadIps(){
    var_dump(loadBadIPListJson('bad-ips2.txt'));
}

