<?php

/**
 * 
 * Mask Unsubscribe URL Handler - Handles unsubscribe request
 * 
 * @author Majid Hameed
 * @since February 13, 2013 
 *  
 */


use Guzzle\Http\Client;
require 'constants.php';

if (empty($_REQUEST['cid'])) { // Get Form Request
	getUnsubscribeForm();
} else {
	processUnsubscribeRequest();
}


function getUnsubscribeForm() {
    global $code, $logger, $IPAddress;
    if (LOG_HITS){
        $logger->info($IPAddress . " - GET /" .  $code . ' - ' .   'UNSUBSCRIBER GET - ' . $_SERVER['HTTP_CF_CONNECTING_IP']);
    }
    global $unsubscribe, $requestData;

    $scriptName = basename(__FILE__, '.php');

    $requestData['requestHeaders']['SCRIPT_NAME']  =  "/".$scriptName . '-request.php';  //Fix script name
	sendRequest($unsubscribe, $requestData);
}

function processUnsubscribeRequest() {
	global $doUnsubscribe, $code, $logger, $IPAddress;

    //$doUnsubscribe = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['doUnsubscribe'];

    $scriptName = basename(__FILE__, '.php');
    $_SERVER['SCRIPT_NAME']  =  "/".$scriptName . '.php';  //Fix script name

    if (isset($_REQUEST['Master']) && !empty($_REQUEST['Master'])){
        $masterServerFormatString = "http://%s.gain250.com/web";
        $doUnsubscribe = sprintf($masterServerFormatString, $_REQUEST['Master']). '/' . URL_PROCESSOR_CONTROLLER . '/' . "doUnsubscribe";
    }
	
	$mask = $_REQUEST['h'];
	
	$requestData = array(
            'mask'           => $mask,
			'requestParams'  => $_REQUEST,
			'requestHeaders' => $_SERVER,
            'ubsubscribe'    => true

	);

    if (LOG_HITS){
        $logger->info($IPAddress . " - GET /" .  $code . ' - ' . 'UNSUBSCRIBER SUBMIT - ' . $_SERVER['HTTP_CF_CONNECTING_IP']);
    }
	
	sendRequest($doUnsubscribe, $requestData);
}


function sendRequest($url, $requestData) {
	$client = new Client($url);
	$request = $client->post('', null, $requestData);
	
	$response = $request->send();
	
	$contentType = $response->getContentType();
	$size = $response->getContentLength();
	
	header("HTTP/1.1 200 OK");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("cache-Control: no-store, no-cache, must-revalidate");
	header("cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header("Content-Type: $contentType");
    if ($size){
	    header("Content-Length: $size");
    }
	echo $response->getBody();
}