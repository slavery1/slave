<?php
/**
 * Created by PhpStorm.
 * User: yancy
 * Date: 4/04/17
 * Time: 8:39
 */

$url_handler_constants_p = parse_ini_file('url-handler-constants.ini', true);

$URL_PROCESSORS = $url_handler_constants_p['URL_PROCESSORS'];

$u_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['u'];
$i_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['i'];
$t_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['t'];
$r_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['r'];
$o_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['o'];
$h_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['h'];

$ga_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['ga'];

//$util_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['get'];

$g_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['g'];

$unsubscribe = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['unsubscribe'];
$unsubscribe2 = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['unsubscribe2'];
$doUnsubscribe = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['doUnsubscribe'];
$doUnsubscribe2 = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['doUnsubscribe2'];

$ipBotsUrl = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['IPBots'];
$badImgUrl = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['BadImgUrl'];

