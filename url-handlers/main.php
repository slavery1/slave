<?php
/**
 * Created by PhpStorm.
 * User: yancy
 * Date: 5/09/14
 * Time: 8:51
 */

require_once 'vendor/autoload.php';
require 'constants.php';
require 'common.php';
require 'utils.php';
use Guzzle\Http\Client;

ini_set('display_errors', 'Off');

$scriptName = basename(__FILE__, '.php');

//echo json_encode( decrypt($code) );


$IPAddress = '';
if (isset($_SERVER['REMOTE_ADDR'])){
    $IPAddress = $_SERVER['REMOTE_ADDR'];
}

if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])){
    $IPAddress = $_SERVER['HTTP_CF_CONNECTING_IP'];
}

$code = $_REQUEST['c1'] ."/". $_REQUEST['c2'];

$code = str_replace(" ", "+", $code);


if (LOG_HITS){
    //$logger->info($IPAddress . " - GET /" .  $code . " - MAIN");
}


require_once 'filter_bots.php';

try{
// Process Request


if (empty($_REQUEST['c1']) || (BOT_FILTERING_ENABLED && isBadUserAgent($_SERVER['HTTP_USER_AGENT'], $BAD_UA_LIST))) {
    if (LOG_HITS){
        $logger->info($IPAddress. " - GET /" . $code . ' - ' . 'BOT AGENT - ' . $_SERVER['HTTP_USER_AGENT']);
    }
    $logger->debug($scriptName . ' - Invalid Request - Request Detail - ' . json_encode($_SERVER));
    return;
}


$decrypt_array = decryptv2( $code );
$version = 2;

if ($decrypt_array == null){ //todo remove it later. it is only for old links compatibility
    $decrypt_array = decrypt($code);
    $version = 1;
}

//var_dump($decrypt_array);
//return;

$subjectID = -1;

if ($version == 2){
    $decodeEmail = decodeEmail($decrypt_array['cs']);

}else{
    $decodeEmail = decodeEmail($decrypt_array['cs'], true);
}

if (!filter_var($decodeEmail, FILTER_VALIDATE_EMAIL)) {
    if (LOG_HITS){
        $logger->info($IPAddress . " - GET /" .  $code . ' - ' . 'BOT INVALID LINK FORMAT - ' . $decodeEmail);
    }
    $logger->debug($scriptName . ' - invalid email or url ' . $decodeEmail);
    return;
}

if ($decrypt_array != false){
    $script = $decrypt_array["pref"];
    $mask = $decrypt_array["mask"];
    $params = $decrypt_array["params"];
    $subjectID = $decrypt_array['trackParams']["SUBJECT"];

    if (count($params) > 0 && $version == 2){
        //Server Name
        $masterName = clearName($params[count($params) - 1]);

        //Check if master is in servers
        if (!empty($MASTER_NAMES) && !in_array($masterName, $MASTER_NAMES)){
            //echo "master server $masterName is not autorize to send";
            if (LOG_HITS){
                $logger->info($IPAddress . " - GET /" .  $code . ' - '. 'BOT MASTER NAME - ' .  $decodeEmail);
            }
            return;
        }

        unset($params[count($params) - 1]);

        define('MASTER_SERVER_NAME', $masterName);
        define('MASTER_SERVER_URL', sprintf($url_handler_constants['MASTER_SERVER_URL_POSFIX'], $masterName));
    }else{
        define('MASTER_SERVER_URL', $url_handler_constants['MASTER_SERVER_URL']);
        define('MASTER_SERVER_NAME', '');
    }

    //Load constants
    require 'processors_constants.php';

    //echo MASTER_SERVER_URL;
    //return;
}




//Check for possible bots
if (isPossibleBot($IPAddress)){

    if (LOG_HITS){
        $logger->info($IPAddress . " - GET /" . $code . ' - '. 'BOT IP - ' .  $_SERVER['HTTP_USER_AGENT']);
    }

    $logger->debug($scriptName . ' - Possible bot IP - Request Detail - ' . json_encode($_SERVER));
    //$script = "x";
    return;
}


//$mask = $_REQUEST['h'];

$_SERVER['QUERY_STRING'].="&cs=".$decrypt_array['cs'];
$_SERVER['SUBJECT_ID'] = $subjectID;

$requestData = array(
    'mask' => $mask,
    'requestHeaders' => $_SERVER
);

$logger->debug($scriptName . ' - Valid Request - Request Data - ' . json_encode($requestData));

// POST data to be saved in database
//$client = new Client($g_processor);
}catch (Exception $e) {
    //$logger->error($e->getMessage());
    display_500();
}

try {
    $originalURL = "/" . $script . "/" . $mask . "/" . $decrypt_array['cs'];

    if ( $script == "u" ){
        require_once 'u.php';
    }
    if ( $script == "t" ){
        require_once 't.php';
    }
    if ( $script == "i" ){
        require_once 'i.php';
    }

    if ( $script == "x" ){
        require_once 'unsubscribe.php';
    }

    if ( $script == "r" ){
        require_once 'r.php';
    }

    if ( $script == "o" ){
        require_once 'o.php';
    }

    if ( $script == "h" ){
        require_once 'h.php';
    }

    //echo $originalURL;
    //return;

} catch (Exception $e) {
    //$logger->error($e->getMessage());
}



?>