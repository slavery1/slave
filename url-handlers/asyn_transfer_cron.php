<?php
/**
 * Created by PhpStorm.
 * User: yancy
 * Date: 28/02/17
 * Time: 6:15
 */

ini_set('include_path', dirname(__FILE__));

require_once 'vendor/autoload.php';
require 'constants.php';
require 'common.php';
require 'utils.php';
require 'cron_helper.php';
use Guzzle\Http\Client;

$scriptName = basename(__FILE__, '.php');
$scriptTitle = "Transfer cron - ";

try{
    if(($pid = CronHelper::Lock()) !== FALSE) {
        sendToAllMasters();
        CronHelper::Unlock();
    }
}catch(Exception $e) {
    $logger->debug($scriptName . "-" . $e);
    exit(1);
}

function sendToAllMasters(){
    global $MASTER_NAMES, $url_handler_constants;
    $keepOlds = true;
    if (!empty($MASTER_NAMES)){
        foreach(array_unique($MASTER_NAMES) as $masterName){
            $masterUrlServer = sprintf($url_handler_constants['MASTER_SERVER_URL_POSFIX'], $masterName);
            $masterProcessor = $masterUrlServer . URL_PROCESSOR_CONTROLLER . '/' . 'bulk';
            sendBulkHits($masterProcessor, $masterName);
        }
    }

    if ($keepOlds){ //To get old links
        $masterProcessor = $url_handler_constants['MASTER_SERVER_URL'] . URL_PROCESSOR_CONTROLLER . '/' . 'bulk';
        sendBulkHits($masterProcessor);
    }
}


function sendBulkHits($masterProcessor, $masterName=''){
    global $logger, $scriptTitle;
    $serverPref = $masterName != '' ? "[$masterName]" : '[OLD_VERSION]';
    $logger->info($scriptTitle .$serverPref . " ". __FUNCTION__. ": Init");
    $linesToProcess = 1000;
    //$masterProcessor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . 'bulk';

    $data = getHits($linesToProcess, $masterName);
    $total = getTotal($masterName);

    if ($total == 0){
        $logger->info($scriptTitle . "$serverPref Nothing to process...");
    }else{
        $processors = array();
        foreach($data as $d){
            pushData($processors, $d);
        }

        $ids = $processors['ids'];
        $processors['data'] = json_encode($processors['data']);
        array_shift($processors);
        curl_post_async($masterProcessor, $processors, $masterName);
        $logger->info($scriptTitle . "$serverPref Processed " . count($ids) . " of " . $total . " hits ");

        dropHit("(" . join(", ", $ids) . ")", true, $masterName);
    }


    $logger->info($scriptTitle .$serverPref . " ". __FUNCTION__. ": End");
}


function pushData(&$processors, $d){
    $id = $d[0];
    $processor = $d[1];
    $server = $d[2];
    $mask = $d[3];

    $data = array(
        'data' => $server,
        'mask' => $mask
    );

    //Push the data
    $processors['ids'][] = $id;
    $processors['data'][] = $data;
}


function sendHits(){
    global $logger, $scriptTitle;
    $logger->info($scriptTitle . __FUNCTION__ . ": Init");
    $linesToProcess = 1000;
    $data = getHits($linesToProcess);

    foreach($data as $d){
        $id = $d[0];
        $processor = $d[1];
        $data = $d[2];
        $mask = $d[3];
        curl_post_async($processor, json_decode($data), $mask);
        dropHit($id);
        usleep(100);
    }

    $logger->info($scriptTitle . __FUNCTION__. ": End");
}