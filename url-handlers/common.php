<?php
/**
 * Contains common/generic methods for the URL Handlers
 * 
 * @author Majid Hameed
 * @since August 5, 2013
 */

/**
 * Returns true if the user agent is bad false otherwise
 * @param String $ua
 * @param Array $BAD_UA_LIST
 * @return boolean (true if bad user agent false otherwise)
 */
function isBadUserAgent($ua, $BAD_UA_LIST) {
    //$url = 'http://www.user-agents.org/allagents.xml';
    $url = 'bad_agents.xml';
    $BAD_UA_LIST = loadBadUserAgent($url, $BAD_UA_LIST);
	foreach ($BAD_UA_LIST as $badUA) {
        if (strcmp ($ua, $badUA) == 0){
			return true;
		}
	}
	return false;
}

function isPossibleBot($ipAddress){
    $url = 'bad_ips.txt';
    $BAD_IPS_LISR = loadBadIPList($url);
    if (!is_array($BAD_IPS_LISR)) return false;

    if (in_array(trim($ipAddress), $BAD_IPS_LISR)){
        return true;
    }else{
        foreach($BAD_IPS_LISR as $range){
            if (!filter_var($range, FILTER_VALIDATE_IP) && cidr_match(trim($ipAddress), $range)){
                return true;
            }
        }
    }

    return false;
}

/*
function cidr_match($ip, $range){
    list ($subnet, $bits) = explode('/', $range);
    $ip = ip2long($ip);
    $subnet = ip2long($subnet);
    $mask = -1 << (32 - $bits);
    $subnet &= $mask; # nb: in case the supplied subnet wasn't correctly aligned
    return ($ip & $mask) == $subnet;
}*/

function isBadImage($url, $BAD_IMG_LIST){
    foreach ($BAD_IMG_LIST as $badImg){
        if (strcmp ($url, $badImg) == 0){
            return true;
        }
    }
    return false;
}

/**
 * Get bot filter list from local xml of bots
 * @param String $url
 * @param $BAD_UA_LIST
 * @return mixed
 */
function loadBadUserAgent($url, $BAD_UA_LIST){

    $xml = simplexml_load_file($url);
    if ($xml->count() > 0){
        foreach ($xml->children() as $child)
        {
            $string =  (string) $child->String;  //->asXML();
            $type = $child->Type;

            $type = str_split($type);

            if (in_array("R", $type) || in_array("S", $type)){
                array_push($BAD_UA_LIST, $string);
            }
        }
    }
    return $BAD_UA_LIST;
}

function loadBadIPList($url){
    $lines = file($url, FILE_IGNORE_NEW_LINES);
    return $lines;
}

function loadBadIPListJson($url){
    $the_data = file_get_contents($url);
    return json_decode($the_data);
}

function writeFile($fileName, $fileContent, $mode="w"){
    $result = false;
    $file = fopen($fileName, $mode);
    if ($file) {
        if(fwrite($file, $fileContent) ){
            $result = true;
        }
        fclose($file);
    }
    return $result;
}