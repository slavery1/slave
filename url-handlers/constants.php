<?php
/**
 * Holds constants for URL Handlers
 * @author Majid Hameed
 * 
 */

$url_handler_constants = parse_ini_file('url-handler-constants.ini', true);
$bad_img = parse_ini_file('bad_imgs.ini', true);

//define('MASTER_SERVER_URL', $url_handler_constants['MASTER_SERVER_URL']);
define('URL_PROCESSOR_CONTROLLER', $url_handler_constants['URL_PROCESSOR_CONTROLLER']);
define('BOT_FILTERING_ENABLED',	(bool) $url_handler_constants['BOT_FILTERING_ENABLED']);
define('JS_REDIRECT_ENABLED', (bool) $url_handler_constants['JS_REDIRECT_ENABLED']);
//define('PIXEL_IMG', (bool) $url_handler_constants['PIXEL_IMG']);
define('URL_SERVER_CONTROLLER', $url_handler_constants['URL_SERVER_CONTROLLER']);


$BAD_UA_LIST = $url_handler_constants['BAD_UA_LIST'];

$BAD_IMG_LIST = $bad_img['BAD_IMGS'];

$requestOptions = $url_handler_constants['HTTP_REQUEST_OPTIONS'];

Logger::configure(dirname(__FILE__). '/log4php.xml');
// Fetch a logger, it will inherit settings from the root logger
$logger = Logger::getLogger('url-handlers');

//Logger::configure(dirname(__FILE__). '/log4phphits.xml');
//$logger1 = Logger::getLogger('url-handlers');

//Keys that are going to be tracked
//$TRACK_KEYS = array("HOSTNAME", "SUBJECT");
$TRACK_KEYS = array("SUBJECT");

$MASTER_NAMES = isset($url_handler_constants['MASTER_SERVER']) ? $url_handler_constants['MASTER_SERVER']: array();

define('BULK_OPEN', false);
define('BULK_CLICK', false);

define('LOG_HITS', false);