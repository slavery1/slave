#!/usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------------------------------------------------------
# Name:        tunnel.py
# Purpose:     Make tunnel configuration
# Author:      Rolando Caner
# Created:     2016/08/16
# --------------------------------------------------------------------------------------

import os

# get id of the slave
_id = 0
data = os.popen("ip add sh | grep tun | grep inet").readlines()
for item in data:
    newid = int(item.split()[6].strip("tun").split("_")[0])
    if newid > _id:
        _id = newid
print "slave id: %s" % _id

# get current routes
routes = dict()
data = os.popen("ip ro sh | grep tun | grep via").readlines()
for item in data:
    line = item.split()
    routes[line[0]] = line[2]
print "routes  :",
print routes

# get current tunnels
if _id != 0:
    data = os.popen("ip add sh | grep tun | grep inet").readlines()
    for item in data:
        newid = int(item.split()[6].strip("tun").split("_")[0])
        if newid == _id:
            line = item.split()
            n = line[1].split(".")
            net = "%s.%s.0.0/16" % (n[0], n[1])
            gat = line[3].split("/")[0]

            # check if the route is present
            if net in routes.keys() and routes[net] == gat:
                pass
            else:
                cmd = "ip ro add %s via %s" % (net, gat)
                print cmd
                print "Route not found"
                os.system(cmd)
